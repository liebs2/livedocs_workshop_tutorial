# LiveDocs Workshop Tutorial

## Badges

### Public CRC Binderhub Links
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://c109-005.cloud.gwdg.de:30901/v2/gwdg/liebs2%2Flivedocs_workshop_tutorial/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://c109-005.cloud.gwdg.de:30901/v2/gwdg/liebs2%2Flivedocs_workshop_tutorial/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://c109-005.cloud.gwdg.de:30901/v2/gwdg/liebs2%2Flivedocs_workshop_tutorial/HEAD?urlpath=voila)

### JupyterLite Link
[![lite-badge](https://img.shields.io/badge/CRC1456-Jupyterlite-yellow)](https://liebs2.pages.gwdg.de/livedocs_workshop_tutorial)

### Static HTMLs
[![static-html](https://img.shields.io/badge/CRC1456-Hello_World-white)](https://pedrocklein.pages.gwdg.de/livedocs_workshop_tutorial/files/hello_world.html)

### Docker Image at GWDG Gitlab Docker Registry
[![Docker](https://img.shields.io/badge/CRC1456-Dockerhub-blue)](https://gitlab.gwdg.de/liebs2/livedocs_workshop_tutorial/container_registry/3054)

